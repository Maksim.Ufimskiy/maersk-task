# Maersk Task README

## Installation and Running solution
The solution contains 2 parts: backend and frontend.

In order to start backend App user should `cd backend` and run `yarn` or `npm install` to download and install dependencies.<br />
After installation user can use `npm start` command to run NodeJS backend. 

In order to start frontend user should `cd frontend` and run `yarn` or `npm install` to download and install dependencies.<br />
After installation user can use `npm start` command to run Frontend on dev server.
Use `npm build` to create frontend build.

##Description

The task solution is divided in 2 parts. Backend app is made for proxying Github api as most convenient way to pass access token, gather data and logs.
Another reason is that I implemented OAuth for authorization. I registered this app at Github where I got clientId, secret code and I specified Authorization callback URL. It points to `http://localhost:8080/auth/github/callback`.

`http://localhost:8080` is where backend served when you run `npm run` in backend folder.<br />
`backend/logs/github.log` - log file.<br />

####Backend API:
Endpoints with required Github access token: <br />
`/api/profile` - returns data for current authorized user <br />
`/api/profile/:userName` - returns data for user with login=userName<br />
`/api/profiles?q={userName}` - endpoint for searching users by userName query<br />
`/api/profile/:userName/repos` - return list of {userName} repos<br />
`/api/profile/:userName/repos/:repo` return {userName} repo <br />
`/api/profile/:userName/events` - return user events <br />

Public Endpoints without required Github access token: <br />
`/api/logs` - return backend logs <br/>
`/api/healthcheck` - return backend health <br/>

####Frontend

Frontend was implemented using React(create-react-app) because it is a very convenient way to start small project like this. I added react-material-ui because I haven't used it before and I wanted an opportunity to try it and learn how it works. The same reason I used C3js to render charts.
 
 
 #####Authorization Workflow
 Frontend app is loaded and there's a request to backend to get data about logged in user. If there is no Authorization token in request header or it is invalid then backend returns `401 unauthorized`. Then user is redirected to Github login page where he enters his credentials. After that auth callback triggered and github passes code to backend which allows backend to make request to github API and obtain access token which afterwards passed to frontend. Once token is received Frontend saves it to localStorage and adds it to every request to backend API.
 
#####PS
Commits chart is working but sometimes it is hard to find a user who committed a lot in a year. <br/>

 
 Please let me know if you have any questions, I'll be glad to answer.<br/>
 My contacts: <br/>
 email: maksim.ufimskiy@gmail.com<br/>
 Skype: imaxim88<br/>
 Mobile: +79058744757<br/>
 
 I look forward to hearing from you.
 <br/>
 <br/>
 Kind regards,<br/>
 Maksim Ufimskiy 
   