import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import request from 'request';
import morgan from 'morgan';
import fs from 'fs';
import path from 'path';
import expressHealthcheck from 'express-healthcheck';

import {authHandler} from './routes/authHandler';
import {profileHandler} from './routes/profileHandler';
import {profileSearchHandler} from './routes/profileSearchHandler';
import {profileByIdHandler} from "./routes/profileByIdHandler";
import {userReposHandler} from "./routes/userReposHandler";
import {userRepoCommitsHandler} from "./routes/userRepoCommitsHandler";
import {userEventsHandler} from "./routes/userEventsHandler";
import {logsHandler} from "./routes/logsHandler";

module.exports = class App {

    getExpressServer() {
        const logFolder = 'logs/';
        let server = express();

        server.use(cors({
            origin: '*',
            optionsSuccessStatus: 200
        }));

        server.use(bodyParser.json());
        server.use(bodyParser.urlencoded({extended: true}));

        if (!fs.existsSync(logFolder)){
            fs.mkdirSync(logFolder);
        }

        const accessLogStream = fs.createWriteStream(path.join(logFolder, 'github.log'), {flags: 'a'});

        server.use(morgan('combined', {stream: accessLogStream}));
        server.use(['/api/profile*', '/api/profiles*'], authHandler);

        server.use('/api/healthcheck', expressHealthcheck());

        server.get('/auth/github/callback', (req, res) => {
            request.post({
                url: 'https://github.com/login/oauth/access_token',
                form: {
                    client_id: 'aeba7db130f7edba586d',
                    client_secret: '5f994328dd1aaf0704e0c39c46e81f093f0ec6f2',
                    code: req.query.code
                },
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }, (err, httpResponse, body) => {
                err && res.status(500).send(err);
                res.redirect('http://localhost:3000/auth?' + body);
            })
        });

        server.get('/api/profile', profileHandler);
        server.get('/api/profile/:userName', profileByIdHandler);
        server.get('/api/profiles', profileSearchHandler);
        server.get('/api/profile/:userName/repos', userReposHandler);
        server.get('/api/profile/:userName/repos/:repo', userRepoCommitsHandler);
        server.get('/api/profile/:userName/events', userEventsHandler);
        server.get('/api/logs', logsHandler);

        return server;
    }
};