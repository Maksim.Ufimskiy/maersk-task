import request from 'request';

export function userReposHandler(req, res) {
    const authHeader = req.headers['authorization'];
    const splittedAuthHeader = authHeader && authHeader.split(' ') || [];
    const headers = {
        'Authorization': 'token ' + splittedAuthHeader[1],
        'User-Agent': 'maersk-task'
    };
    resendingRequest(req, res, headers);
}

function resendingRequest(req, res, headers) {
    request.get({
        url: `https://api.github.com/users/${req.params.userName}/repos`,
        headers
    }, (err, httpResponse, body) => {
        if (httpResponse.statusCode === 202){
            setTimeout(() => resendingRequest(req, res, headers), 100);
        } else {
            err && res.status(500).send(err);
            res.send(body);
        }
    });
}