import fs from 'fs';
import path from 'path';

export function logsHandler (req, res) {
    const logPath = path.join('logs/', 'github.log');

    fs.stat(logPath, (err, stats) => {
        if (!err) {
            fs.createReadStream(logPath).pipe(res);
        } else {
            res.send('No logs found');
        }
    });
}