import request from 'request';

export function profileSearchHandler(req, res) {
    const authHeader = req.headers['authorization'];
    const splittedAuthHeader = authHeader && authHeader.split(' ') || [];
    request.get({
        url: `https://api.github.com/search/users?q=${req.query.q}`,
        headers: {
            'Authorization': 'token ' + splittedAuthHeader[1],
            'User-Agent': 'maersk-task'
        }
    },(err, httpResponse, body) => {
        err && res.status(500).send(err);
        res.send(body);
    });
}