import request from 'request';

export function profileHandler(req, res) {
    const authHeader = req.headers['authorization'];
    const splittedAuthHeader = authHeader && authHeader.split(' ') || [];
    request.get({
        url: 'https://api.github.com/user',
        headers: {
            'Authorization': 'token ' + splittedAuthHeader[1],
            'User-Agent': 'maersk-task'
        }
    }, (err, httpResponse, body) => {
        err && res.status(500).send(err);
        res.send(body);
    });
}