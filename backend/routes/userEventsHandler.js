import request from 'request';

export function userEventsHandler(req, res) {
    const authHeader = req.headers['authorization'];
    const splittedAuthHeader = authHeader && authHeader.split(' ') || [];
    const headers = {
        'Authorization': 'token ' + splittedAuthHeader[1],
        'User-Agent': 'maersk-task'
    };
    request.get({
        url: `https://api.github.com/users/${req.params.userName}/received_events/public`,
        headers: {
            'Authorization': 'token ' + splittedAuthHeader[1],
            'User-Agent': 'maersk-task'
        }
    }, (err, httpResponse, body) => {
        err && res.status(500).send(err);
        res.send(body);
    });
}
