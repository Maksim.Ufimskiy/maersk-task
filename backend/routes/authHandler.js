export function authHandler (req, res, next) {
    const authHeader = req.headers['authorization'];
    const splittedAuthHeader = authHeader && authHeader.split(' ') || [];
    if (splittedAuthHeader[0] === 'Bearer' && splittedAuthHeader[1]) {
        next();
    } else {
        res.status(401).send('unauthorized');
    }
}