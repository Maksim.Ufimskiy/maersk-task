require('babel-core/register');

const App = require('./app');
const app = new App();
const port = process.env.PORT || 8080;
const server = app.getExpressServer();
server.listen(port, () => {
    console.log(`App listening on port ${port}!`)
});