import React, { Component } from 'react';
import './App.css';
import { getCurrentUser, getUsersById } from './services/resources';
import { withStyles } from 'material-ui/styles';
import queryString from 'query-string';

import AppHeader from './components/AppHeader/AppHeader';
import UsersGridList from './components/UsersGridList/UsersGridList';
import BusyLoader from './components/BusyLoader/BusyLoader';

const styles = {
    card: {
        maxWidth: 250,
    },
    media: {
        height: 180,
    },
    title: {
        display: 'flex'
    },
    loadingScreen: {
        width: '100%',
        height: '100%',
        display: 'flex',
        'align-items': 'center',
        'justify-content': 'center'
    },
    loadingLabel: {
        paddingRight: '10px',
        lineHeight: '40px'
    }
};

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            appLoading: true,
            usersLoading: false,
            searchResults: [],
            anchorEl: null
        };
        this.fetchUsers = this.fetchUsers.bind(this);
    }

    fetchUsers(value) {
        this.setState({
            usersLoading: true
        });
        getUsersById(value).then(data => {
            this.props.history.push({
                search: `?userName=${value}`
            });
            this.setState({
                usersLoading: false,
                searchResults: data.items
            });
        });
    }
    componentWillMount() {
        this.setState({
            appLoading: true
        });
        getCurrentUser().then(data => {
            !!data && this.setState({
                currentUser: data
            });
        }).finally(() => {
            this.setState({
                appLoading: false
            });
        });
    }
    render() {
        let content;
        const query = queryString.parse(this.props.location.search) || {};
        if (this.state.appLoading) {
            content = <BusyLoader />;
        } else {
            content = (<div className="App">
                         <AppHeader currentUser={this.state.currentUser} searchHandler={this.fetchUsers} query={query.userName} />
                         <UsersGridList users={this.state.searchResults} loading={this.state.usersLoading}/>
                       </div>);
        }
        return (
            <div className="App-container">
                {content}
            </div>
        );
    }
}

export default withStyles(styles)(App);
