import axios from 'axios';
import { config } from './config';

export function getCurrentUser() {
    return axios.get(config.backendEndpoint + '/api/profile').then((res) => {
        return res.data;
    }).catch((error) => {
        console.log(error.response);
    });
}

export function getUsersById(userSearch) {
    return axios.get(config.backendEndpoint + `/api/profiles?q=${userSearch}`).then((res) => {
        return res.data;
    }).catch((error) => {
        console.log(error.response);
    });
}

export function getUserById(userId) {
    return axios.get(config.backendEndpoint + `/api/profile/${userId}`).then((res) => {
        return res.data;
    }).catch((error) => {
        console.log(error.response);
    });
}

export function getUserRepos(userId) {
    return axios.get(config.backendEndpoint + `/api/profile/${userId}/repos`).then((res) => {
        return res.data;
    }).catch((error) => {
        console.log(error.response);
    });
}

export function getUserRepoCommit(userId, repo) {
    return axios.get(config.backendEndpoint + `/api/profile/${userId}/repos/${repo}`).then((res) => {
        return res.data;
    }).catch((error) => {
        console.log(error.response);
    });
}

export function getUserActivities(userId) {
    return axios.get(config.backendEndpoint + `/api/profile/${userId}/events`).then((res) => {
        return res.data;
    }).catch((error) => {
        console.log(error.response);
    });
}