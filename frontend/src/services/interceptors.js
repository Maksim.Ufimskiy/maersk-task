import axios from 'axios';
import {config} from './config';

export function initInterceptors() {
    axios.interceptors.request.use((config) => {
        const token = localStorage.getItem('maersk-token') || '';
        config.headers = {
            'Authorization': 'Bearer ' + token
        };
        return config;
      }, function (error) {
        return Promise.reject(error);
      });
    
    axios.interceptors.response.use((response) => {
        if (response.data && response.data.message === 'Bad credentials') {
            window.location = config.githubAuthEndpoint;
        }
        return response;
      }, function (error) {
        if (error.response.status === 401) {
            window.location = config.githubAuthEndpoint;
        }
    
        return Promise.reject(error);
      });
}