import 'typeface-roboto';
import React from 'react';
import ReactDOM from 'react-dom';
import { Route } from 'react-router';
import { BrowserRouter } from 'react-router-dom';

import registerServiceWorker from './registerServiceWorker';

import App from './App';
import Auth from './components/Auth/Auth';
import Profile from './components/Profile/Profile';
import './index.css';
import {initInterceptors} from './services/interceptors';

initInterceptors();

ReactDOM.render(
    <BrowserRouter>
        <div className="container">
            <Route exact path="/" component={App} />
            <Route excat path="/auth" component={Auth} />
            <Route excat path="/profile/:userName" component={Profile} />
        </div>
    </BrowserRouter>
    , document.getElementById('root'));
registerServiceWorker();
