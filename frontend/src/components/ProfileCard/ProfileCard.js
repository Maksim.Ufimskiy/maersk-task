import React from 'react';
import { withStyles } from 'material-ui/styles';
import { Link } from 'react-router-dom';
import Card, { CardHeader, CardActions, CardContent, CardMedia } from 'material-ui/Card';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';

const styles = {
    card: {
        maxWidth: 250,
        boxShadow: 'none'
    },
    cardHeader: {
        paddingTop: '5px',
        textAlign: 'center'
    },
    cardContent: {
        paddingBottom: '0',
        textAlign: 'center'
    },
    cardActions: {
        'justify-content': 'space-around'
    },
    media: {
        height: 180
    },
};

function ProfileCard(props) {
    const { classes, currentUser = {} } = props;
    return (
        <Card className={classes.card}>
            <CardHeader
                className={classes.cardHeader}
                title={currentUser.name || currentUser.login}
                subheader={currentUser.bio}
            />
            <CardMedia
                className={classes.media}
                image={currentUser.avatar_url}
                title="Contemplative Reptile"
            />
            <CardContent className={classes.cardContent}>
                <Typography component="div">
                    {currentUser.company}
                </Typography>
                <Typography component="div">
                    {currentUser.location}
                </Typography>
                <Typography component="div">
                    {currentUser.email}
                </Typography>
            </CardContent>
            <CardActions className={classes.cardActions}>
                <Button dense color="primary">
                    <Link to={{pathname: `/profile/${currentUser.login}`}}>
                        Profile
                    </Link>
                </Button>
            </CardActions>
        </Card>
    );
}

export default withStyles(styles)(ProfileCard);