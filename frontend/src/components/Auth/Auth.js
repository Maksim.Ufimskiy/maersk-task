import React, { Component } from 'react';
import queryString from 'query-string';
import BusyLoader from '../BusyLoader/BusyLoader';

export default class Auth extends Component {
    componentWillMount() {
        const query = queryString.parse(this.props.location.search);
        localStorage.setItem('maersk-token', query.access_token || null);
        this.props.history.push('/');
    }
    render() {
        return (
            <BusyLoader label="Logging in"/>
        );
    }
}
