import React from 'react';
import { withStyles } from 'material-ui/styles';
import Typography from 'material-ui/Typography';
import { CircularProgress } from 'material-ui/Progress';

const styles = {
    title: {
        display: 'flex'
    },
    loadingScreen: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        backgroundColor: 'rgba(255, 255, 255, 0.8)',
        display: 'flex',
        'align-items': 'center',
        'justify-content': 'center',
        'z-index': 10000
    },
    loadingLabel: {
        paddingRight: '10px',
        lineHeight: '40px'
    }
};

function BusyLoader(props) {
    const { classes, label = 'Loading' } = props;
    return (
        <div className={classes.loadingScreen}>
            <Typography type="title" color="inherit" className={classes.title}>
                <span className={classes.loadingLabel}>{label}</span>
                <CircularProgress className={classes.progress} />
            </Typography>
        </div>
    );
}

export default withStyles(styles)(BusyLoader);