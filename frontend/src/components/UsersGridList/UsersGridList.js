import React from 'react';
import {withStyles} from 'material-ui/styles';
import GridList, {GridListTile, GridListTileBar} from 'material-ui/GridList';
import { Link } from 'react-router-dom';
import Subheader from 'material-ui/List/ListSubheader';
import IconButton from 'material-ui/IconButton';
import InfoIcon from 'material-ui-icons/Info';
import Typography from 'material-ui/Typography';
import BusyLoader from '../BusyLoader/BusyLoader';

const styles = theme => ({
    container: {
        position: 'relative',
        height: 'calc(100% - 64px)'
    },
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'auto',
        backgroundColor: theme.palette.background.paper
    },
    gridList: {
        width: '80%',
        overflow: 'hidden'
    },
    icon: {
        color: 'rgba(255, 255, 255, 0.54)'
    },
    noResults: {
        height: '100%',
        position: 'absolute',
        display: 'flex',
        'align-items': 'center'
    }
});

function UsersGridList(props) {
    const {users = [], loading, classes} = props;
    return (
        <div className={classes.container}>
            <div className={classes.root}>
                {users.length > 0 && (<GridList cellHeight={180} className={classes.gridList} cols={4}>
                    <GridListTile key="Subheader" cols={4}
                                  style={{height: 'auto', visibility: users.length > 0 ? 'visible' : 'hidden'}}>
                        <Subheader component="div">Found Users</Subheader>
                    </GridListTile>
                    {users.map(user => (
                        <GridListTile key={user.id}>
                            <img src={user.avatar_url} alt={user.login}/>
                            <GridListTileBar
                                title={user.login}
                                subtitle={<span>Score: {user.score}</span>}
                                actionIcon={
                                    <Link to={{pathname: `/profile/${user.login}`}}>
                                        <IconButton className={classes.icon}>
                                            <InfoIcon/>
                                        </IconButton>
                                    </Link>
                                }
                            />
                        </GridListTile>
                    ))}
                </GridList>)}
                {users.length === 0 && (<div className={classes.noResults}>
                    <Typography type="display1" gutterBottom>No users found</Typography >
                </div>)}
            </div>
            {loading && <BusyLoader />}
        </div>
    );
}

export default withStyles(styles)(UsersGridList);