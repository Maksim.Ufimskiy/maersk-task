import React from 'react';
import {withStyles} from 'material-ui/styles';
import Card, { CardContent, CardMedia } from 'material-ui/Card';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import Table, { TableBody, TableCell, TableRow } from 'material-ui/Table';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';
import KeyboardBackspace from 'material-ui-icons/KeyboardBackspace';
import Button from 'material-ui/Button';
import c3 from 'c3';
import '../../../node_modules/c3/c3.css';
import _ from 'lodash';

import {getUserById, getUserRepos, getUserRepoCommit, getUserActivities} from "../../services/resources";
import BusyLoader from '../BusyLoader/BusyLoader';

const styles = theme => ({
    root: {
        minHeight: '90vh',
        marginBottom: '25px'
    },
    card: {
        display: 'flex',
    },
    infoLabel: {
        display: 'inline-block',
        width: '70px',
        paddingRight: '10px'
    },
    chartContainer: {
        margin: '15px',
        position: 'relative'
    },
    chart: {
        height: '320px'
    },
    chartTitle: {
        textAlign: 'center'
    },
    details: {
        display: 'flex',
        flexDirection: 'column',
    },
    content: {
        flex: '1 0 auto',
    },
    userNameContainer: {
        marginBottom: '15px'
    },
    cover: {
        width: 180,
        height: 180
    },
    profileContainer: {
        margin: '0 auto',
        maxWidth: '800px',
        paddingTop: '40px'
    },
    goBack: {
        position: 'fixed',
        top: '20px',
        left: '20px'
    }
});

const usersChartConfig = {
    bar: {
        width: {
            ratio: 0.5
        }
    },
    axis: {
        x: {
            type: 'category',
            tick: {
                rotate: 75,
                multiline: false
            },
            height: 130
        }
    }
};

const commitsChartConfig = {
    bar: {
        width: {
            ratio: 0.5
        }
    }
};

class Profile extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            tabIndex: 0,
            randomRepo: ''
        };
        this.handleTabSelect = this.handleTabSelect.bind(this);
    }
    handleTabSelect(event, tabIndex) {
        this.setState({ tabIndex });
        if (tabIndex === 0) {
            setTimeout(() => {
                !this.state.commitsChartLoading && (this.commitsChart = renderChart('#commitsChart', {
                    columns: this.state.commitsChartColumns,
                    type: 'bar'
                }, commitsChartConfig));
                !this.state.usersChartLoading && (this.usersChart = renderChart('#usersChart', {
                    x: 'x',
                    columns: this.state.userChartColumns,
                    type: 'bar'
                }, usersChartConfig));
            });
        }
    }
    componentDidMount() {
        this.setState({
            loading: true,
            commitsChartLoading: true,
            usersChartLoading: true
        });
        getUserById(this.props.match.params.userName).then((user) => {
            this.setState({
                user
            });
            getUserActivities(user.login).then(activities => {
                const users = Object.values(activities.reduce((result, event) => {
                    if (result[event.actor.login]) {
                        result[event.actor.login].count++;
                    } else {
                        result[event.actor.login] = {
                            login: event.actor.login,
                            count: 1
                        };
                    }
                    return result;
                }, {})).sort((a, b) => {

                    return a.count < b.count ? 1 : -1;
                });
                let userChartColumns = _.take(users.map((user) => user.count), 10);
                let ticks = _.take(users.map((user) => user.login), 10);
                userChartColumns =[
                    ['x', ...ticks],
                    ['Events', ...userChartColumns]
                ];
                this.usersChart = renderChart('#usersChart', {
                    x: 'x',
                    columns: userChartColumns,
                    type: 'bar'
                }, usersChartConfig);
                this.setState({
                    usersChartLoading: false,
                    userChartColumns
                });
            });
            return getUserRepos(user.login);
        }).then((repos) => {
            const randomRepo = repos[Math.floor(Math.random() * repos.length)].name;
            this.setState({
                repos,
                randomRepo
            });
            return getUserRepoCommit(this.state.user.login, randomRepo);
        }).then((commits) => {
            let commitsChartColumns = commits.map(commit => commit.total) || [];
            commitsChartColumns = [['Commits', ...commitsChartColumns]];
            this.commitsChart = renderChart('#commitsChart', {
                columns: commitsChartColumns,
                type: 'bar'
            }, commitsChartConfig);
            this.setState({
                commits,
                commitsChartColumns,
                commitsChartLoading: false
            });
        }).finally(() => {
            this.setState({
                loading: false
            });
        });
    }

    render() {
        const {user = {}, tabIndex, randomRepo, commitsChartLoading,  usersChartLoading } = this.state;
        const {classes, history } = this.props;
        return (
            <div className={classes.profileContainer}>
                <div className={classes.goBack}>
                    <Button fab color="secondary" aria-label="back" onClick={history.goBack}>
                        <KeyboardBackspace />
                    </Button>
                </div>
                <Paper className={classes.root} elevation={4}>
                    <Card className={classes.card}>
                        <CardMedia
                            className={classes.cover}
                            image={user.avatar_url}
                            title="User's Avatar"
                        />
                        <div className={classes.details}>
                            <CardContent className={classes.content}>
                                <Typography type="headline" className={classes.userNameContainer}>
                                    <span>{user.name || user.login}</span>
                                </Typography>
                                {user.email && (<Typography type="subheading" color="textSecondary">
                                    <span className={classes.infoLabel}>Email: </span><span>{user.email}</span>
                                </Typography>)}
                                {user.company && (<Typography type="subheading" color="textSecondary">
                                    <span className={classes.infoLabel}>Company: </span><span>{user.company}</span>
                                </Typography>)}
                                {user.location && (<Typography type="subheading" color="textSecondary">
                                    <span className={classes.infoLabel}>Location: </span><span>{user.location}</span>
                                </Typography>)}
                            </CardContent>
                        </div>
                    </Card>
                    <AppBar position="static">
                        <Tabs value={tabIndex} onChange={this.handleTabSelect}>
                            <Tab label="Charts" />
                            <Tab label="Details" />
                        </Tabs>
                    </AppBar>
                    {tabIndex === 0 && <div>
                                            <div className={classes.chartContainer}>
                                                <Typography className={classes.chartTitle} type="subheading">Commits per week for <span>{randomRepo}</span> repo</Typography>
                                                {commitsChartLoading && (<BusyLoader />)}
                                                <div className={classes.chart} id="commitsChart"></div>
                                            </div>
                                            <div className={classes.chartContainer}>
                                                <Typography className={classes.chartTitle} type="subheading">Most frequent users from events</Typography>
                                                {usersChartLoading && (<BusyLoader />)}
                                                <div className={classes.chart} id="usersChart"></div>
                                            </div>
                                        </div>}
                    {tabIndex === 1 && <div>
                            <Table className={classes.table}>
                                <TableBody>
                                    {Object.keys(user).reduce((result, key) =>{
                                        user[key] && result.push({key, value: user[key]});
                                        return result;
                                    }, []).map((item) => {
                                        return (
                                            <TableRow key={item.key}>
                                                <TableCell><div>{item.key}</div></TableCell>
                                                <TableCell><div>{item.value}</div></TableCell>
                                            </TableRow>
                                        );
                                    })}
                                </TableBody>
                            </Table>
                    </div>}
                </Paper>
            </div>
        );
    }
}

export default withStyles(styles)(Profile);

function renderChart(containerId, data, config) {
    return c3.generate({
        bindto: containerId,
        data,
        ...config
    });
}