import React from 'react';
import {withStyles} from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/IconButton';
import AccountCircle from 'material-ui-icons/AccountCircle';
import Menu from 'material-ui/Menu';
import {MuiThemeProvider, createMuiTheme} from 'material-ui/styles';
import Input from 'material-ui/Input';
import ProfileCard from '../ProfileCard/ProfileCard';

const styles = {
    root: {
        display: 'flex',
        width: '100%',
        textAlign: 'left'
    },
    toolbar: {
        'justify-content': 'space-between'
    },
    title: {
        width: '170px'
    },
    input: {
        flex: 0.7
    },
    accountIcon: {
        justifyContent: 'flex-end'
    },
    accountMenu: {
        padding: 0
    }

};

const theme = createMuiTheme({
    palette: {
        type: 'dark'
    },
});

class AppHeader extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            anchorEl: null
        };
        this.handleUserSearch = this.handleUserSearch.bind(this);
        this.handleMenu = this.handleMenu.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    handleMenu(event) {
        this.setState({anchorEl: event.currentTarget});
    }

    handleUserSearch(event) {
        clearTimeout(this.typeTimer);
        const search = event.target.value;
        this.typeTimer = setTimeout(() => this.props.searchHandler(search), 500);
    }
    handleEnterInput(event) {
        (event.keyCode === 13 && event.target.value) && this.props.searchHandler(event.target.value);
    }
    componentWillMount() {
        this.typeTimer = null;
    }
    componentDidMount() {
        if (this.props.query) {
            this.userNameInput.value = this.props.query;
            this.props.searchHandler(this.props.query);
        }
    }
    handleClose() {
        this.setState({anchorEl: null});
    }

    render() {
        const {classes, query = '', currentUser = {}} = this.props;
        const {anchorEl} = this.state;
        const open = Boolean(anchorEl);
        return (
            <div className={classes.root}>
                <AppBar position="static">
                    <Toolbar className={classes.toolbar}>
                        <Typography type="title" color="inherit" className={classes.title}>
                            Maersk Task
                        </Typography>
                        <MuiThemeProvider theme={theme}>
                            <Input
                                placeholder="Enter Github User ID"
                                color="primary"
                                className={classes.input}
                                onChange={this.handleUserSearch}
                                onKeyDown={(event) => this.handleEnterInput(event)}
                                ref={(input) => { this.userNameInput = input; }}
                                defaultValue={query}
                                inputProps={{
                                    'aria-label': 'Description',
                                }}
                            />
                        </MuiThemeProvider>
                        <IconButton
                            aria-owns={open ? 'menu-appbar' : null}
                            aria-haspopup="true"
                            onClick={this.handleMenu}
                            color="inherit"
                            className={classes.accountIcon}
                        >
                            <AccountCircle/>
                        </IconButton>
                        <Menu
                            id="menu-appbar"
                            className={classes.accountMenu}
                            anchorEl={anchorEl}
                            anchorOrigin={{
                                vertical: 'top',
                                horizontal: 'right'
                            }}
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'right'
                            }}
                            open={open}
                            onClose={this.handleClose}
                        >
                            <ProfileCard currentUser={currentUser}/>
                        </Menu>
                    </Toolbar>
                </AppBar>
            </div>
        );
    }
}

export default withStyles(styles)(AppHeader);